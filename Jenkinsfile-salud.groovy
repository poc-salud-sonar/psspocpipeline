pipeline {

    agent {
        kubernetes {
            yamlFile 'Salud.yaml'
        }
    }

    options {
        timeout(time: 1, unit: 'HOURS')
    }

    stages {

        stage('Print Parameters') {
            steps {
                echo "Parameters: BRANCH=${env.BRANCH}"
            }
        }

        stage('Start Sonarqube') {
            steps {
                echo "Start Sonarqube in background"
                container('docker') {
                    script {
                        try {
                            sh("""
                                echo "Kill sonarqube server if running... "                                
                                docker rm poc-salud-sonarqube -f                                                
                            """)
                        } catch (err) {
                            echo err.getMessage()
                            currentBuild.result = 'SUCCESS'
                        }
                    }
                    sh("""
                        echo "Start sonarqube server in background... please check time to start up"                        
                        docker run -d --name poc-salud-sonarqube -e SONAR_ES_BOOTSTRAP_CHECKS_DISABLE=true -p 9000:9000 sonarqube:8.2-community                        
                    """)
                }
            }
        }

        stage('Download sonar-cnes-report') {
            steps {
                sh("""                
                    echo "Download tool for build reports"
                    curl -L https://github.com/cnescatlab/sonar-cnes-report/releases/download/3.3.1/sonar-cnes-report-3.3.1.jar --output sonar-cnes-report.jar
                """)
            }
        }

        stage('Scanning Angular') {
            steps {
                echo "Checkout code from scm... "
                dir('Angular') {
                    git(
                            url: 'git@bitbucket.org:poc-salud-sonar/psspocfe.git',
                            credentialsId: 'APP-ESPJENKINS-PRO',
                            branch: "${env.BRANCH}"
                    )
                    container('nodejs') {
                        dir ("MapfrePssPocFE.src") {
                            sh("""                        
                                echo "Install java on container nodejs"
                                apk add openjdk11
                                echo "Install app-angular and sonar-scanner"                        
                                npm install --force
                                echo "Fix permissions"
                                chmod +x \$(pwd)/node_modules/sonar-scanner/bin/sonar-scanner
                                echo "Start Code Analisis"
                                npm run sonarqube
                            """)
                        }
                    }
                    container('maven11') {
                        sh("""                
                            sleep 30
                            echo "Generate new reports"
                            java -jar ../sonar-cnes-report.jar -s http://172.17.0.1:9000 -p Mapfre.PSS.Angular -o reports
                        """)
                    }
                    sh("""
                        echo "Zip reports"
                        tar -czvf sonarqube-reports-angular.tar.gz reports
                        ls -lah
                    """)
                    archiveArtifacts artifacts: 'sonarqube-reports-angular.tar.gz'
                }
            }
        }

        stage('Scan Code Backend') {
            steps {
                echo "Checkout code from scm... "
                dir('Backend') {
                    git(
                            url: 'git@bitbucket.org:poc-salud-sonar/psspocbe.git',
                            credentialsId: 'APP-ESPJENKINS-PRO',
                            branch: "${env.BRANCH}"
                    )
                    container('maven') {
                        dir ("MapfrePssPocBE.src") {
                            sh("""
                                echo "Compile"                        
                                mvn clean compile
                            """)
                        }
                    }
                    container('maven11') {
                        dir ("MapfrePssPocBE.src") {
                            sh("""   
                                echo "Start Code Analisis"
                                export MAVEN_OPTS="-Xmx2048m"                     
                                mvn sonar:sonar -Dsonar.host.url=http://172.17.0.1:9000                                
                            """)
                        }
                        sh("""                                   
                                sleep 30                                
                                echo "Generate new reports"
                                java -jar ../sonar-cnes-report.jar -s http://172.17.0.1:9000 -p Mapfre.PSS.Angular.Backend -o reports
                            """)
                    }
                    sh("""
                        echo "Zip reports"
                        tar -czvf sonarqube-reports-backend.tar.gz reports                    
                    """)
                    archiveArtifacts artifacts: 'sonarqube-reports-backend.tar.gz'
                }
            }
        }

    }

    post {
        always {
            echo "Stop Sonarqube"
            container('docker') {
                script {
                    try {
                        sh("""
                                echo "Kill sonarqube server if running... "                                
                                docker rm poc-salud-sonarqube -f                                                
                            """)
                    } catch (err) {
                        echo err.getMessage()
                        currentBuild.result = 'SUCCESS'
                    }
                }
            }
        }
    }
}
